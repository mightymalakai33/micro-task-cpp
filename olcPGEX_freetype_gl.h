#pragma once

#include <freetype-gl/freetype-gl.h>
#include <freetype-gl/vertex-buffer.h>

#include "olcPixelGameEngine.h"

namespace olc {
	class FreeType_GL : public olc::PGEX {
	private:
		texture_atlas_t* atlas = nullptr;
		texture_font_t* font = nullptr;
		vertex_buffer_t* buffer = nullptr;

	public:
		FreeType_GL(const std::string& path, float font_size);
		FreeType_GL(const std::string& path, float font_size, size_t atlas_size, size_t atlas_depth);

		void draw_string(const std::string& string, int x, int y, olc::Pixel color, float angle);

		int height() const { return (int)font->height; }
		int width() const
		{
			texture_glyph_t* glyph = texture_font_get_glyph(font, "W");

			if (glyph != nullptr)
			{
				return glyph->advance_x;
			}
			return 0;
		}
	private:
		void draw_bitmap(float x, float y, const texture_glyph_t& glyph, olc::Pixel color);
	};
};

#ifdef OLC_PGEX_FREETYPE_GL

olc::FreeType_GL::FreeType_GL(const std::string& path, float font_size)
{
	atlas = texture_atlas_new(512, 512, 3);
	font = texture_font_new_from_file(atlas, font_size, path.c_str());
}

olc::FreeType_GL::FreeType_GL(const std::string& path, float font_size, size_t atlas_size, size_t atlas_depth)
{
	atlas = texture_atlas_new(atlas_size, atlas_size, atlas_depth);
	font = texture_font_new_from_file(atlas, font_size, path.c_str());
}


void olc::FreeType_GL::draw_string(const std::string& string, int x, int y, olc::Pixel color, float angle)
{
	srand(1234);

	float pos_x = (float)x;
	float pos_y = (float)y;

	for (size_t i = 0; i < string.size(); ++i)
	{
		const char ch = string[i];

		texture_glyph_t* glyph = texture_font_get_glyph(font, &ch);

		if (glyph != nullptr)
		{
			draw_bitmap(pos_x, pos_y, *glyph, color);

			pos_x += glyph->advance_x;
			pos_y += glyph->advance_y;
		}
	}

}

void olc::FreeType_GL::draw_bitmap(float x, float y, const texture_glyph_t& glyph, olc::Pixel color)
{
	// TODO different render modes
	static bool first = true;

	olc::Pixel pixel = olc::Pixel(rand() % 255, rand() % 255, rand() % 255);

	if (first)
	{
		std::cout << font->height << " " << glyph.advance_x << " " << glyph.advance_y << " " << glyph.offset_x << " " << glyph.offset_y << "\n";
	}

	/*x += glyph.offset_x;
	y += glyph.offset_y;*/
	//float kerning = texture_glyph_get_kerning(*glyph, )
	int texture_x = (int)(glyph.s0 * atlas->width);
	int texture_y = (int)(glyph.t0 * atlas->height);

	int start = (texture_y * atlas->width * atlas->depth) + texture_x * atlas->depth;

	for (size_t bx = 0; bx < glyph.width; bx++)
	{
		for (size_t by = 0; by < glyph.height; by++)
		{
			
			unsigned char p = atlas->data[start + (by * atlas->width * atlas->depth) + bx * atlas->depth];
			//olc::Pixel pixel = p == 0 ? olc::BLACK : olc::WHITE;
			////olc::Pixel pixel = olc::WHITE;
			//bool val = (p >> (7 - bx % 8)) & 1;
			//if (val) {
			//	//pge->Draw(x + bx, y + by, color);
			if (p != 0)
			{
				color.a = p;
				pge->Draw((int32_t)(x + glyph.offset_x + bx), (int32_t)(y + (font->height - glyph.offset_y) + by), color);
			}

			//}
			
		}
	}
	first = false;
}

#endif
