#pragma once

#include "micro_task_structures.h"

#include <string>
#include <regex>



// add
// start
// stop
// finish
Tasks& get_tasks() ;

void run_command(std::ostream& out, const std::string& input);
