#include "task_writer.h"

void Task_Writer::write_task(const Task& task, std::ostream& output)
{
	output << task.m_name << '\n';
	output << task.m_state << '\n';
}
