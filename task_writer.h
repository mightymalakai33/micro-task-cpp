#pragma once

#include "micro_task_structures.h"

class Task_Writer
{
public:
	void write_task(const Task& task, std::ostream& output);
};
