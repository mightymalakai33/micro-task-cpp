#include <freetype-gl/freetype-gl.h>

#define OLC_PGE_APPLICATION
#include "olcPixelGameEngine.h"

#define OLC_PGEX_FREETYPE_GL
#include "olcPGEX_freetype_gl.h"






#include "micro_task.h"
#include "console.h"

#include <iostream>






// recreate micro-task java version in c++
// build terminal features from the olcPGEX freetype gl code
//		scrolling buffer of lines
//		ability to have graphics embedded
//		
class Micro_Task_App : public olc::PixelGameEngine
{
private:
	olc::FreeType_GL freetype_gl_1;
	olc::FreeType_GL freetype_gl_3;
	olc::FreeType_GL big_font;
	olc::FreeType_GL small_font;

	Console m_console;

public:
	Micro_Task_App()
		: freetype_gl_1("consola.ttf", 26, 512, 1),
		  freetype_gl_3("consola.ttf", 22, 512, 3),
		  big_font("consola.ttf", 40),
		  small_font("arialbd.ttf", 22)
	{
		sAppName = "Example";
	}

public:
	bool OnUserCreate() override
	{
		// Called once at the start, so create things here
		return true;
	}

	bool OnUserUpdate(float fElapsedTime) override
	{
		Clear(olc::BLACK);

		bool shift = GetKey(olc::Key::SHIFT).bHeld;

		const auto add = [&](olc::Key key, char lower, char upper) {
			if (GetKey(key).bPressed)
			{
				m_console.add_char(shift ? upper : lower);
			}
		};

		add(olc::Key::A, 'a', 'A');
		add(olc::Key::B, 'b', 'B');
		add(olc::Key::C, 'c', 'C');
		add(olc::Key::D, 'd', 'D');
		add(olc::Key::E, 'e', 'E');
		add(olc::Key::F, 'f', 'F');
		add(olc::Key::G, 'g', 'G');
		add(olc::Key::H, 'h', 'H');
		add(olc::Key::I, 'i', 'I');
		add(olc::Key::J, 'j', 'J');
		add(olc::Key::K, 'k', 'K');
		add(olc::Key::L, 'l', 'L');
		add(olc::Key::M, 'm', 'M');
		add(olc::Key::N, 'n', 'N');
		add(olc::Key::O, 'o', 'O');
		add(olc::Key::P, 'p', 'P');
		add(olc::Key::Q, 'q', 'Q');
		add(olc::Key::R, 'r', 'R');
		add(olc::Key::S, 's', 'S');
		add(olc::Key::T, 't', 'T');
		add(olc::Key::U, 'u', 'U');
		add(olc::Key::V, 'v', 'V');
		add(olc::Key::W, 'w', 'W');
		add(olc::Key::X, 'x', 'X');
		add(olc::Key::Y, 'y', 'Y');
		add(olc::Key::Z, 'z', 'Z');

		add(olc::Key::K0, '0', ')');
		add(olc::Key::K1, '1', '!');
		add(olc::Key::K2, '2', '@');
		add(olc::Key::K3, '3', '#');
		add(olc::Key::K4, '4', '$');
		add(olc::Key::K5, '5', '%');
		add(olc::Key::K6, '6', '^');
		add(olc::Key::K7, '7', '&');
		add(olc::Key::K8, '8', '*');
		add(olc::Key::K9, '9', '(');

		add(olc::Key::NP0, '0', '0');
		add(olc::Key::NP1, '1', '1');
		add(olc::Key::NP2, '2', '2');
		add(olc::Key::NP3, '3', '3');
		add(olc::Key::NP4, '4', '4');
		add(olc::Key::NP5, '5', '5');
		add(olc::Key::NP6, '6', '6');
		add(olc::Key::NP7, '7', '7');
		add(olc::Key::NP8, '8', '8');
		add(olc::Key::NP9, '9', '9');

		add(olc::Key::MINUS, '-', '_');
		add(olc::Key::EQUALS, '=', '+');

		add(olc::Key::COMMA, ',', '<');
		add(olc::Key::PERIOD, '.', '>');
		add(olc::Key::SPACE, ' ', ' ');

		add(olc::Key::OEM_1, ';', ':'); // ; :
		add(olc::Key::OEM_2, '/', '?'); // / ?
		add(olc::Key::OEM_3, '`', '~'); // ` ~
		add(olc::Key::OEM_4, '[', '{'); // [ {
		add(olc::Key::OEM_5, '\\', '|'); // \ |
		add(olc::Key::OEM_6, ']', '}'); // ] }
		add(olc::Key::OEM_7, '\'', '\"'); // ' "
		//oem(olc::Key::OEM_8);

		
		if (GetKey(olc::Key::ENTER).bPressed || GetKey(olc::Key::RETURN).bPressed)
		{
			const std::string& line = m_console.current_line();
			m_console.add_char('\n');

			// TODO I don't like writing to std::ostringstream, turning that into std::istringstream and then pumping that into console
			std::ostringstream ss;
			run_command(ss, line);

			std::istringstream in{ ss.str() };
			std::string str;
			while (std::getline(in, str))
			{
				m_console.add_line(str);
			}
		}

		if (GetKey(olc::Key::BACK).bPressed)
		{
			m_console.remove_char();
		}

		m_console.render(0, 0, 1600, 900);


		std::optional<Task> active_task = get_tasks().active_task();

		if (active_task.has_value())
		{
			big_font.draw_string(std::to_string(current_time() - active_task->m_add_time), 0, 0, olc::GREY, 0.0f);
		}

		return true;
	}
};


int main()
{
	Micro_Task_App demo;
	if (demo.Construct(1600, 900, 1, 1))//, false, true, false))
	//	if (demo.Construct(256, 240, 4, 2))
		demo.Start();
	return 0;
}
//int main()
//{
//	std::string str;
//	do {
//		std::getline(std::cin, str);
//		run_command(std::cout, str);
//	} while (str != "exit");
//	
//}
