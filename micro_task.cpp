#include "micro_task.h"

#include <iostream>

Tasks tasks;
std::regex command_split(" (?=([^\"]*\"[^\"]*\")*[^\"]*$)");

// hidden function (not in header) that builds the proper command class for the command and subcommands
[[nodiscard]] std::unique_ptr<Base_Command> create_command(const std::string& command, const std::vector<std::string> &subcommands)
{
	if (command == "add")
	{
		if (subcommands.size() > 0 && subcommands.at(0) == "task")
		{
			return std::make_unique<Add_Task_Command>(tasks);
		}
	}
	else if (command == "start")
	{
		if (subcommands.size() > 0 && subcommands.at(0) == "task")
		{
			return std::make_unique<Start_Task_Command>(tasks);
		}
	}
	else if (command == "stop")
	{
		if (subcommands.size() > 0 && subcommands.at(0) == "task")
		{
			return std::make_unique<Stop_Task_Command>(tasks);
		}
	}
	else if (command == "finish")
	{
		if (subcommands.size() > 0 && subcommands.at(0) == "task")
		{
			return std::make_unique<Finish_Task_Command>(tasks);
		}
	}

	return nullptr;
}

Tasks& get_tasks()  { return tasks; }

void run_command(std::ostream& out, const std::string& input)
{
	//std::regex_match(split, command)
	// split string by spaces, eventually leaving strings alone


	//std::cout << "running command: " << command << '\n';



	std::sregex_token_iterator iter(input.begin(), input.end(), command_split, -1);
	std::sregex_token_iterator end;

	if (iter == end)
	{
		return;
	}

	const std::string command = *iter;
	++iter; // read the command, move on

	std::vector<std::string> subcommands;
	std::vector<std::string> args;

	bool read_an_argument = false;

	for (; iter != end; ++iter)
	{
		// we only allow subcommands at the start, unlike picocli
		if (!read_an_argument && (iter->str().front() >= 'a' && iter->str().front() <= 'z'))
		{
			subcommands.push_back(*iter);
		}
		else
		{
			read_an_argument = true;
			args.push_back(*iter);
		}
	}

	out << "running: '" + command + "'";
	out << " with subcommands: { ";

	for (const std::string& sub : subcommands)
	{
		out << "'" + sub + "' ";
	}
	out << "} with arguments: { ";

	for (const std::string& arg : args)
	{
		out << "'" + arg +  "' ";
	}
	out << "}\n";

	// test outputing a lot of data to our console
	for (int i = 0; i < 10000; i++)
	{
		//out << std::to_string(i) << " ";
	}
	// find the right command, fail if it's not a valid command


	// anything without --  or - is probably a subcommand, check those

	// run the command
	auto command_instance = create_command(command, subcommands);

	if (command_instance != nullptr)
	{
		command_instance->run_command(out, args);
	}
	else
	{

	}
}
