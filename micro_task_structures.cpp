#include "micro_task_structures.h"

#include <iostream>

void Add_Command::run_command(std::ostream& out, const std::vector<std::string>& args)
{
}

// TODO learn how std::span works and maybe use it here instead of begin, end
void Add_Task_Command::run_command(std::ostream& out, const std::vector<std::string>& args)
{
	// consume first argument, which should be the name of the task
	const std::string& task_name = args.at(0);

	Task_Add_Params params;
	params.m_name = task_name;

	// add the task
	int64_t id = m_tasks.add_task(params);
	out << "Added task " << id << '\n';
}

void Start_Task_Command::run_command(std::ostream& out, const std::vector<std::string>& args)
{
	int64_t id = std::atoi(args.at(0).c_str());

	m_tasks.start_task(id);

	out << "Started task " << id << '\n';
}

void Stop_Task_Command::run_command(std::ostream& out, const std::vector<std::string>& args)
{
	int64_t id = std::atoi(args.at(0).c_str());

	m_tasks.start_task(id);

	out << "Stopped task " << id << '\n';
}

void Finish_Task_Command::run_command(std::ostream& out, const std::vector<std::string>& args)
{
	int64_t id = std::atoi(args.at(0).c_str());

	m_tasks.start_task(id);

	out << "Finished task " << id << '\n';
}
