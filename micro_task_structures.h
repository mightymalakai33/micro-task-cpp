#pragma once

#include <cstdint>
#include <limits>
#include <string>
#include <vector>
#include <memory>
#include <chrono>
#include <sstream>
#include <optional>

#pragma region Tasks

enum class Task_State
{
	inactive,
	active,
	finished
};

inline std::ostream& operator<<(std::ostream& out, Task_State state)
{
	switch (state)
	{
	//using enum Task_State;
	case Task_State::inactive:
		out << "Inactive";
		break;
	case Task_State::active:
		out << "Active";
		break;
	case Task_State::finished:
		out << "Finished";
		break;
	}
	return out;
}

constexpr int64_t time_not_set = INT_MAX;

struct Task_Times
{
	int64_t start;
	int64_t stop = time_not_set;
};

struct Task_Add_Params
{
	std::string m_name = "";
	Task_State m_state = Task_State::inactive;
	int64_t m_add_time = 0;
	int64_t m_due_time = time_not_set;
	bool m_recurring = false;
};

struct Task
{
	int64_t m_id;
	std::string m_name;
	Task_State m_state = Task_State::inactive;
	int64_t m_add_time;
	int64_t m_finish_time = time_not_set;
	bool m_recurring = false;
	int64_t m_due_time = time_not_set;

	std::vector<Task_Times> m_start_stop_times;
	std::vector<std::string> m_tags;

	Task(int64_t id, std::string name, int64_t add_time) : m_id(id), m_name(std::move(name)), m_add_time(add_time) {}

	[[nodiscard]] Task_State state() const { return m_state; }
	[[nodiscard]] int64_t add_time() const { return m_add_time; }
	[[nodiscard]] int64_t finish_time() const { return m_finish_time; }

	void start_task(int64_t start)
	{
		m_state = Task_State::active;
	
		m_start_stop_times.push_back({ start });
	}

	void stop_task(int64_t stop)
	{
		m_state = Task_State::inactive;
	
		Task_Times times{ m_start_stop_times.back().start, stop };
		m_start_stop_times.pop_back();
		m_start_stop_times.push_back(times);
	}

	void finish_task(int64_t finish)
	{
		if (m_state == Task_State::active)
		{
			stop_task(finish);
		}

		m_state = Task_State::finished;

		m_finish_time = finish;
	}
};

struct Container_Path
{
	std::vector<std::string> containers;
	std::string full_path;
	bool is_group;

	std::string this_container;

	Container_Path(std::string path)
		: full_path(std::move(path)),
		is_group(full_path.back() == '/')
	{
		std::istringstream ss(full_path);

		std::string temp;

		while (getline(ss, temp, '/'))
		{
			containers.push_back(temp);
		}

		this_container = containers.back();
	}
};

struct Task_Container
{
	Container_Path m_path;

	Task_Container(Container_Path path)
		: m_path(path)
	{
	}
};

inline int64_t current_time()
{
	using namespace std::chrono;

	system_clock::time_point tp = system_clock::now();
	system_clock::duration dtn = tp.time_since_epoch();

	return dtn.count() * system_clock::period::num / system_clock::period::den;
}

struct Task_List : Task_Container
{
//private:
	std::vector<Task> m_tasks;

public:
	Task_List(Container_Path path) : Task_Container(path) {}

	void add_task(int64_t id, Task_Add_Params params)
	{
		m_tasks.emplace_back(id, params.m_name, current_time());
	}

	void start_task(int64_t id)
	{
		auto it = std::find_if(m_tasks.begin(), m_tasks.end(), [&id](Task t) { return t.m_id == id; });

		if (it != m_tasks.end())
		{
			it->start_task(current_time());
		}
	}

	void stop_task(int64_t id)
	{
		auto it = std::find_if(m_tasks.begin(), m_tasks.end(), [&id](Task t) { return t.m_id == id; });

		if (it != m_tasks.end())
		{
			it->stop_task(current_time());
		}
	}

	void finish_task(int64_t id)
	{
		auto it = std::find_if(m_tasks.begin(), m_tasks.end(), [&id](Task t) { return t.m_id == id; });

		if (it != m_tasks.end())
		{
			it->finish_task(current_time());
		}
	}
};

struct Task_Group : Task_Container
{
	std::vector<std::unique_ptr<Task_Container>> m_children;

	Task_Group(Container_Path path) : Task_Container(path) {}

	void add_container(std::unique_ptr<Task_Container>&& container)
	{
		m_children.push_back(std::move(container));
	}
};

constexpr int64_t no_active_task = -1;
constexpr int64_t invalid_task_id = -1;

struct Active_Context
{
	// TODO This will get slow eventually when we have a ton of tasks
	int64_t active_task = no_active_task;

	Task_List* active_list = nullptr;
	Task_Group* active_group = nullptr;
	//Task* active_task = nullptr;
	//int64_t 
};

#pragma endregion

#pragma region Utilities

class Task_Reader
{
private:
	std::ostream& m_input;

public:
	Task_Reader(std::ostream& input) : m_input(input)
	{
	}

	~Task_Reader() = default;

	void read_task()
	{

	}
};

class Tasks
{
private:
	int64_t m_next_id = 1;

	Task_Group m_root;

	Active_Context m_active_context;

public:
	Tasks() : m_root(Container_Path("/"))
	{
		m_root.add_container(std::make_unique<Task_List>(Container_Path("/default")));

		m_active_context.active_list = find_list_for_path(Container_Path("/default"));
	}

	[[nodiscard]] std::optional<Task> active_task() const
	{
		if (m_active_context.active_task != no_active_task)
		{
			Task_List* list = find_list_for_task(m_active_context.active_task);

			if (list != nullptr)
			{
				for (const auto& task : list->m_tasks)
				{
					if (task.m_id == m_active_context.active_task)
					{
						return task;
					}
				}
			}
		}
		return {};
	}
	

	[[nodiscard]] int64_t add_task(Task_Add_Params params)
	{
		return add_task(m_active_context.active_list->m_path, params);
	}

	[[nodiscard]] int64_t add_task(const Container_Path& path, Task_Add_Params params)
	{
		Task_List* list = find_list_for_path(path);

		if (list != nullptr)
		{
			int64_t id = m_next_id++;

			if (m_next_id % 100 == 0)
			{
				m_next_id++;
			}

			list->add_task(id, params);

			return id;
		}
		return invalid_task_id;
	}

	void start_task(int64_t id)
	{
		Task_List* list = find_list_for_task(id);

		if (list != nullptr)
		{
			list->start_task(id);

			m_active_context.active_task = id;
		}
	}

	void stop_task(int64_t id)
	{
		Task_List* list = find_list_for_task(id);

		if (list != nullptr)
		{
			list->stop_task(id);

			m_active_context.active_task = no_active_task;
		}
	}

	void finish_task(int64_t id)
	{
		Task_List* list = find_list_for_task(id);

		if (list != nullptr)
		{
			list->finish_task(id);

			m_active_context.active_task = no_active_task;
		}
	}

private:
	Task_List* find_list_for_path(const Container_Path& path)
	{
		Task_Container* container = &m_root;

		// 0 is "" which is root
		for (size_t i = 1; i < path.containers.size(); i++)
		{
			if (container->m_path.is_group)
			{
				container = find_container(*(Task_Group*)container, path.containers.at(i));
			}
			else
			{
				// we've gone too far and found a list, so we didn't find what we're looking for
				container = nullptr;
			}

			// container wasn't found, exit
			if (container == nullptr)
			{
				return nullptr;
			}
		}

		if (!container->m_path.is_group)
		{
			return (Task_List*)container;
		}
		return nullptr;
	}

	Task_List* find_list_for_task(int64_t id) const
	{
		const Task_Group* group = &m_root;

		for (const auto& child : group->m_children)
		{
			if (child->m_path.is_group)
			{
				Task_List* list = find_list_for_task(id);
				
				if (list != nullptr)
				{
					return list;
				}
			}
			else
			{
				Task_List* list = (Task_List*)child.get();

				for (const auto& task : list->m_tasks)
				{
					if (task.m_id == id)
					{
						return list;
					}
				}
			}
		}

		return nullptr;
	}

	Task_Container* find_container(const Task_Group& group, const std::string& str)
	{
		for (const auto& child : group.m_children)
		{
			if (str == child->m_path.this_container)
			{
				return child.get();
			}
		}
		return nullptr;
	}
};

#pragma endregion

#pragma region Commands

class Base_Command
{
public:
	virtual ~Base_Command() = default;

	virtual void run_command(std::ostream& out, const std::vector<std::string>& args) = 0;
};

class Add_Command : public Base_Command
{
public:

	void run_command(std::ostream& out, const std::vector<std::string>& args) override;
};

class Add_Task_Command : public Add_Command
{
private:
	Tasks& m_tasks;

public:
	Add_Task_Command(Tasks& tasks) : m_tasks(tasks) {}

	void run_command(std::ostream& out, const std::vector<std::string>& args) final;
};


class Start_Command : public Base_Command
{
public:
	void run_command(std::ostream& out, const std::vector<std::string>& args) override {};

};

class Start_Task_Command : public Start_Command
{
private:
	Tasks& m_tasks;

public:
	Start_Task_Command(Tasks& tasks) : m_tasks(tasks) {}

	void run_command(std::ostream& out, const std::vector<std::string>& args) final;
};

class Stop_Command : public Base_Command
{

};

class Stop_Task_Command : public Stop_Command
{
private:
	Tasks& m_tasks;

public:
	Stop_Task_Command(Tasks& tasks) : m_tasks(tasks) {}

	void run_command(std::ostream& out, const std::vector<std::string>& args) final;
};

class Finish_Command : public Base_Command
{
public:
	void run_command(std::ostream& out, const std::vector<std::string>& args) override {};

};

class Finish_Task_Command : public Finish_Command
{
private:
	Tasks& m_tasks;

public:
	Finish_Task_Command(Tasks& tasks) : m_tasks(tasks) {}

	void run_command(std::ostream& out, const std::vector<std::string>& args) final;
};

#pragma endregion
