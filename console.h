#pragma once

#include <vector>
#include <string>

class Console
{
private:
	std::vector<std::string> m_buffer;
	olc::FreeType_GL m_font;

	int m_font_height;
	int m_font_width;

public:
	Console() : m_font("consola.ttf", 22)
	{
		m_buffer.push_back("");

		m_font_height = m_font.height();
		m_font_width = m_font.width();
	}

	std::string current_line() const
	{
		return m_buffer.back();
	}

	void add_char(char ch)
	{
		if (ch == '\n')
		{
			m_buffer.push_back("");
			return;
		}
		std::string& str = m_buffer.back();
		str += ch;
		m_buffer[m_buffer.size() - 1] = str;
	}

	void add_line(const std::string& str)
	{
		m_buffer[m_buffer.size() - 1] = str;
		m_buffer.push_back("");
	}

	void remove_char()
	{
		std::string& str = m_buffer[m_buffer.size() - 1];
		
		if (!str.empty())
		{
			str.erase(str.end() - 1);
		}
	}

	void render(const int x, const int y, const int width, const int height)
	{
		// start at the bottom of the console, m_buffer is ordered so that the lowest line is at end() and highest line is at begin()

		int pos_x = x;
		int pos_y = y + height - m_font_height;

		//for (const auto& str : m_buffer)
		for (auto it = m_buffer.rbegin(); it != m_buffer.rend(); ++it)
		{
			m_font.draw_string(*it, pos_x, pos_y, olc::WHITE, 0.0f);
			pos_y -= m_font_height;

			if (pos_y <= 0)
			{
				break;
			}
		}
	}
};
